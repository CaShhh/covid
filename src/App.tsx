import React from 'react';
import './App.css';
import ChartPicker from '@/components/ChartPicker';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <h2>Latest Covid statistics in Hungary</h2>
      <div className="grid-container">
        <ChartPicker />
        <p>Initial load may take a few seconds </p>
      </div>
      </header>
    </div>
  );
}

export default App;
