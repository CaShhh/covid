import { ChangeEvent, FC, useCallback, useEffect, useState } from 'react';
import { ChartPicker as View } from '@/components/ChartPicker/ChartPicker.view';
import { GETCovidStatsHistory } from '@/redux/actions';
import { useDispatch } from 'react-redux';
import Chart from '@/components/Chart';
import PieChart from '@/components/PieChart';
import LineChart from '@/components/LineChart';
import ChartHistory from '@/components/ChartHistory';

interface Props {
  
}

export const useChartPicker = () => {
  const [selected, setSelected] = useState("bar");
  const dispatch = useDispatch();
  console.log("RRENDER SELECT Input");

  useEffect(() => {
    dispatch(GETCovidStatsHistory());
  }, [dispatch]);

  const onChange = useCallback((event: ChangeEvent<HTMLSelectElement>) => {
    setSelected(event.target.value)
    //dispatch(GETCovidStats()); 
  }, [setSelected]);

  const chart = useCallback(() => {
    switch (selected) {
      case "bar":
        return <ChartHistory />
  
      case "pie":
        return <PieChart />

      case "line":
        return <LineChart />
    
      default:
        return <Chart />
    }
  },[selected])

  return {
    selected,
    onChange,
    chart,
  };
};

export const ChartPicker: FC<Props> = ({ children, ...props }) => {
  const computedProps = useChartPicker(); //itt át lehetne adni neki bármit ami van a componenten
  return <View {...computedProps}>{children}</View>;
};