import { ChangeEventHandler, FC } from 'react';
import Chart from '@/components/Chart';

interface Props {
  selected?: string,
  onChange?: ChangeEventHandler<HTMLSelectElement>;
  chart?: () => JSX.Element;
}

export const ChartPicker: FC<Props> = ({
  chart = () => <Chart />,
  selected,
  onChange,
  children,
  ...Props
}) => (
  <div {...Props}>
    <label htmlFor="charts">Choose a chart: </label>
    <select id="charts" name="charts" onChange={onChange} placeholder={selected}>
      <option value="bar">Bar</option>
      <option value="pie">Pie</option>
      <option value="line">Line</option>
    </select>
    {chart()}
    {children}
  </div>
);