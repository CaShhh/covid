import { FC } from 'react';
import { ICovidStats } from '@/types/covidstats';
import { PieChart as RePieChart, Pie, ResponsiveContainer } from 'recharts';
import './style.css';

interface Props {
  label?:string;
  data? : ICovidStats[]
}

export const PieChart: FC<Props> = ({
  data = [{lastUpdatedAtSource: "", infected: 0, deceased:0, quarantined: 0}],
  label,
  children,
  ...Props
}) => (
  <div className="piechart-container">
    <ResponsiveContainer width="100%" height="100%">
        <RePieChart width={400} height={400}>
        <Pie data={data} dataKey="infected" cx="50%" cy="50%" outerRadius={50} fill="#8884d8" />
        <Pie data={data} dataKey="quarantined" cx="50%" cy="50%" innerRadius={60} outerRadius={70} fill="#82ca9d" />
        <Pie data={data} dataKey="deceased" cx="50%" cy="50%" innerRadius={80} outerRadius={100} fill="#FF0000" label/>
        </RePieChart>
      </ResponsiveContainer>
  </div>
);