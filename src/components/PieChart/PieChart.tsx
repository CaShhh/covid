import { FC } from 'react';
import { PieChart as View } from '@/components/PieChart/PieChart.view';
import { getCovidStatsHistory } from '@/redux/selectors';
import { useSelector } from 'react-redux';

interface Props {
  label?: string,
}

export const usePieChart = () => {
  const PieChart = useSelector(getCovidStatsHistory());
  console.log("RRENDER LineChart");

  return {
    data: PieChart.slice(-3),
  };
};

export const PieChart: FC<Props> = ({ children, ...props }) => {
  const computedProps = usePieChart(); //itt át lehetne adni neki bármit ami van a componenten
  return <View {...computedProps}>{children}</View>;
};