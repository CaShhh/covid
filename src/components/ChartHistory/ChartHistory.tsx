import { FC } from 'react';
import { ChartHistory as View } from '@/components/ChartHistory/ChartHistory.view';
import { getCovidStatsHistory } from '@/redux/selectors';
import { useSelector } from 'react-redux';

export const useChartHistory = () => {
  const ChartHistory = useSelector(getCovidStatsHistory()); //legyen a selectorbol importolva
  console.log("RRENDER ChartHistory");

  return {
    data: ChartHistory.slice(-3),
  };
};

export const ChartHistory: FC = ({ children, ...props }) => {
  const computedProps = useChartHistory(); //itt át lehetne adni neki bármit ami van a componenten
  return <View {...computedProps} {...props}>{children}</View>;
};
