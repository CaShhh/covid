import { FC } from 'react';
import { ICovidStats } from '@/types/covidstats';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import './style.css';

interface Props {
  label?:string;
  data? : ICovidStats[]
}

export const ChartHistory: FC<Props> = ({
  data = [{lastUpdatedAtSource: "", infected: 0, deceased:0, quarantined: 0}],
  children,
  ...Props
}) => (
  <div className="chart-container" {...Props}>
    <ResponsiveContainer width="100%" height="100%">
      <BarChart
        width={500}
        height={300}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="lastUpdatedAtSource" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="infected" fill="#8884d8" />
        <Bar dataKey="deceased" fill="#82ca9d" />
        <Bar dataKey="quarantined" fill="#00ca9d" />
      </BarChart>
    </ResponsiveContainer>
  </div>
);