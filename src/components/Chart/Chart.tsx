import { FC } from 'react';
import { Chart as View } from '@/components/Chart/Chart.view';
import { getCovidStats } from '@/redux/selectors';
import { useSelector } from 'react-redux';

interface Props {
  label?: string,
}

export const useChart = () => {
  const Chart = useSelector(getCovidStats()); //legyen a selectorbol importolva
  console.log("RRENDER Chart");

  return {
    data: [Chart],
  };
};

export const Chart: FC<Props> = ({ children, ...props }) => {
  const computedProps = useChart(); //itt át lehetne adni neki bármit ami van a componenten
  return <View {...computedProps}>{children}</View>;
};