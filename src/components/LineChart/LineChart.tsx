import { FC } from 'react';
import { LineChart as View } from '@/components/LineChart/LineChart.view';
import { getCovidStatsHistory } from '@/redux/selectors';
import { useSelector } from 'react-redux';

interface Props {
  
}

export const useLineChart = () => {
  const LineChart = useSelector(getCovidStatsHistory());
  console.log("RRENDER LineChart");

  return {
    data: LineChart.slice(-3),
  };
};

export const LineChart: FC<Props> = ({ children, ...props }) => {
  const computedProps = useLineChart(); //itt át lehetne adni neki bármit ami van a componenten
  return <View {...computedProps}>{children}</View>;
};