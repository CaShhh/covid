import { FC } from 'react';
import { ICovidStats } from '@/types/covidstats';
import { LineChart as ReLineChart, Line, CartesianGrid, XAxis, YAxis, ResponsiveContainer } from 'recharts';
import './style.css';

export interface Props {
  label?:string;
  data? : ICovidStats[]
}

export const LineChart: FC<Props> = ({
  data = [{lastUpdatedAtSource: "", infected: 0, deceased:0, quarantined: 0}],
  label,
  children,
  ...Props
}) => (
  <div className="linechart-container" {...Props}>
    <ResponsiveContainer width="100%" height="100%">
      <ReLineChart width={400} height={400} data={data}>
      <Line type="monotone" dataKey="infected" stroke="#8884d8" />
      <Line type="monotone" dataKey="deceased" stroke="#FF0000" />
      <Line type="monotone" dataKey="quarantined" stroke="#82ca9d" />
      <CartesianGrid stroke="#ccc" />
      <XAxis dataKey="lastUpdatedAtSource" />
      <YAxis />
      </ReLineChart>
    </ResponsiveContainer>
  </div>
);