export interface ICovidStats {
    infected: number,
    deceased: number,
    quarantined: number,
    lastUpdatedAtSource: string,
}