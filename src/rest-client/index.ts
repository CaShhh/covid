import { ICovidStats } from "@/types/covidstats";

const header = <H extends {}>(header: H = {} as H) => ({
  "Content-type": "application/json",
  ...header
});

const fetchRequest = async <Res>(method: string, url: string) => {
  const res = fetch(url, {
    method,
    headers: header(),
    cache: "default", //nagyon sokáig tölt
  });
  console.log(res);
  return (await res).json() as Promise<Res>;
};

export const getCovidStats = () =>
fetchRequest<ICovidStats>
("GET", "https://api.apify.com/v2/key-value-stores/RGEUeKe60NjU16Edo/records/LATEST?disableRedirect=true?token=XggicZ3sgZyuHAj3pTqvJC6gd");

export const getCovidStatsHistory = () =>
fetchRequest<ICovidStats[]>
("GET", "https://api.apify.com/v2/datasets/Gm6qjTgGqxkEZTkuJ/items?format=json&clean=1&token=XggicZ3sgZyuHAj3pTqvJC6gd");