import { getCovidStats as fetchCovidStats, getCovidStatsHistory as fetchCovidStatsHistory } from "@/rest-client";
import { IGETCovidStats, IGETCovidStatsHistory } from "@/redux/reducer";


export const GETCovidStats = async (): Promise<IGETCovidStats> => {
  try {
    return {
      type: "GET-COVIDSTATS",
      payload: {
        covidStats: (await fetchCovidStats())
      }
    };
  } catch (error) {
    return {
      type: "GET-COVIDSTATS",
      payload: {
        covidStats:{infected: 0, deceased: 0, quarantined: 0, lastUpdatedAtSource: ""}
      }
    };
  }
};

export const GETCovidStatsHistory = async (): Promise<IGETCovidStatsHistory> => {
  try {
    return {
      type: "GET-COVIDSTATSHISTORY",
      payload: {
        covidStatsHistory: (await fetchCovidStatsHistory())
      }
    };
  } catch (error) {
    return {
      type: "GET-COVIDSTATSHISTORY",
      payload: {
        covidStatsHistory:[{infected: 0, deceased: 0, quarantined: 0, lastUpdatedAtSource: ""}]
      }
    };
  }
};
