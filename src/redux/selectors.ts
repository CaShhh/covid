import { Store } from "@/redux/store";

export const getCovidStats = () => (store: Store) => store.covidStats || {infected: 0, deceased: 0, quarantined: 0};
export const getCovidStatsHistory = () => (store: Store) => store.covidStatsHistory || [{infected: 0, deceased: 0, quarantined: 0}];