import { Action } from "redux";
import { Store, initStore } from "@/redux/store";
import { ICovidStats } from "@/types/covidstats";

export interface IGETCovidStats extends Action<"GET-COVIDSTATS"> {
  payload: {
    covidStats: ICovidStats;
  };
}

const getCovidStatsReducer = (
  store: Store,
  { payload: { covidStats } }: IGETCovidStats
): Store => {
  return {
    ...store,
    covidStats
  };
};

export interface IGETCovidStatsHistory extends Action<"GET-COVIDSTATSHISTORY"> {
  payload: {
    covidStatsHistory: ICovidStats[];
  };
}

const getCovidStatsHistoryReducer = (
  store: Store,
  { payload: { covidStatsHistory } }: IGETCovidStatsHistory
): Store => {
  return {
    ...store,
    covidStatsHistory
  };
};

export const reducer = (store: Store = initStore, action: Action) => {
  switch (action.type) {
    case "GET-COVIDSTATS":
      return getCovidStatsReducer(store, action as IGETCovidStats);
    case "GET-COVIDSTATSHISTORY":
      return getCovidStatsHistoryReducer(store, action as IGETCovidStatsHistory);

    default:
      return store;
  }
};
