import { createStore, applyMiddleware } from "redux";
import { asyncMiddleware } from "@/redux/asyncMiddleware";
import { reducer } from "@/redux/reducer";
import { ICovidStats } from "@/types/covidstats";

export const initStore = {
  covidStats: {infected: 0, deceased: 0, quarantined: 0} as ICovidStats,
  covidStatsHistory: [] as ICovidStats[]
};

export type Store = typeof initStore;
export const store = createStore(reducer, applyMiddleware(asyncMiddleware))